<?php


/**
 * Implements hook_views_data().
 */
function sendgrid_inbound_analytics_views_data()
{

    $data = array();

    $data['sendgrid_inbound_analytics_table']['table']['group'] = t('Sendgrid Inbound Analytics');

    $data['sendgrid_inbound_analytics_table']['table']['base'] = array(
        'title' => t('Sendgrid Inbound Analytics'),
        'help' => t('Contains records we want exposed to Views.'),
    );

    // The Serial Number field
	$data['sendgrid_inbound_analytics_table']['serial_no'] = array(
	    'title' => t('Serial Number'),
	    'help' => t('Table Serial Number'),
	    'field' => array(
	        'handler' => 'views_handler_field_numeric',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_numeric',
	    ),
	);

	// The Sendgrid Event ID field
	$data['sendgrid_inbound_analytics_table']['sg_event_id'] = array(
	    'title' => t('Event ID'),
	    'help' => t('Sendgrid Event ID.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

	// The Sendgrid Message ID field
	$data['sendgrid_inbound_analytics_table']['sg_message_id'] = array(
	    'title' => t('Message ID'),
	    'help' => t('The Sendgrid Message ID.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

	// The Sendgrid Useragent field
	$data['sendgrid_inbound_analytics_table']['useragent'] = array(
	    'title' => t('Useragent'),
	    'help' => t('Sendgrid Useragent.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

	// The Sendgrid User Event field
	$data['sendgrid_inbound_analytics_table']['event'] = array(
	    'title' => t('User Event'),
	    'help' => t('Sendgrid User Event.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

	// The Sendgrid User IP field
	$data['sendgrid_inbound_analytics_table']['ip'] = array(
	    'title' => t('User IP'),
	    'help' => t('The Sendgrid User IP.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

		// The Sendgrid User Email field
	$data['sendgrid_inbound_analytics_table']['email'] = array(
	    'title' => t('Email'),
	    'help' => t('Sendgrid User Email.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	    'relationship' => array(
	        'base' => 'users',
	        'field' => 'email',
	        'handler' => 'views_handler_relationship',
	        'label' => t('Users'),
	    ),
	    'argument' => array(
	        'handler' => 'views_handler_argument_users_mail',
	        'validate type' => 'mail',
	    ),
	);

	// The Sendgrid mailkey field
	$data['sendgrid_inbound_analytics_table']['mailkey'] = array(
	    'title' => t('Mailkey'),
	    'help' => t('Sendgrid Mailkey.'),
	    'field' => array(
	        'handler' => 'views_handler_field',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_string',
	    ),
	);

	// The Sendgrid timestamp field
	$data['sendgrid_inbound_analytics_table']['timestamp'] = array(
	    'title' => t('timestamp'),
	    'help' => t('The Sendgrid timestamp.'),
	    'field' => array(
	        'handler' => 'views_handler_field_date',
	    ),
	    'sort' => array(
	        'handler' => 'views_handler_sort_date',
	    ),
	    'filter' => array(
	        'handler' => 'views_handler_filter_date',
	    ),
	);

	$data['sendgrid_inbound_analytics_table']['table']['join'] = array(
	    'users' => array(
	        'left_field' => 'mail',
	        'field' => 'email',
	    ),
	);

    return $data;

}